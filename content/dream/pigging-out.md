+++
categories = ["dreams"]
date = "2017-12-05T01:00:00-04:00"
description = "Wig out 'til you pig out"
disqus_identifier = "/dream//"
disqus_title = ""
disqus_url = "https://dreams.poeti.codes/post//"
images = ["./sandis.png"]
tags = ["late", "parking", "theater", "hog", "quarters"]
title = "Pigging Out"
toc = false

+++
My homies, Will and Matt, drove up to my abode to snag me up. We were trying to make it in time for the movies, so they put the pedal to the metal the moment I hopped in. Our expert driver, Will, went full Tokyo Drift on us as he sped down the highway, weaved through traffic, and hugged the railings of precariously tight turns that I didn't even know Chattanooga had.

Before we knew it, we arrived at a parking garage. It was pretty sparse; plenty of parking for us to choose from. Upon closer inspection, however, we discovered that each parking spot had a parking meter that asked for a flat fee of $0.75 for the night. And before it would even accept any coins, it required a special key to operate the coin slot.

A family who parked right after us saw us struggling and offered us not only their key, but two quarters! We all celebrated and gave them our utmost thanks. A Christmas miracle! Crisis averted!

... Or so we thought! Will & Matt didn't have any other quarters, so we were still down $0.25. I had a quarter, but it was my lucky coin, and I couldn't bare to part with it. It helped me make so many decisions, and I owed it to be decisive on at least this one particular matter. 

We refused to ask the kind family for an additional quarter, as they've already done too much for us. 

We could hear the movie trailers playing off in the distance. The movie was about to start. Time was running short. Options were running low.

And so, I broke my promise. I took out the quarter. Matt and Will were absolutely shocked, for they knew its value to me.

"Heads, it's yours."

I flipped it, with perhaps a bit too much strength as I failed to catch it. The three of us stood around the coin as it spun around on the floor.

Tails.

"It's mine." and I promptly inserted it into the parking meter.

The three of us high-fived each other and entered the movie theater.

... Except for me, because I remembered that I don't really like watching movies. And so I chilled in the parking garage and brainstormed alternative activities that I could engage in.

I had an awesome idea begin to crop up, but before my thought process could finish, I saw a giant hog with a green collar begin to approach me.

And by approach, I meant charge. It went full tilt once we made eye contact.

"Biscuits!"

I ran back into the theater with my figurative tail between my legs, and closed the door. I stood there with my back to the door for a few moments, until I regained my confidence. It's just a wild hog, I thought. I can take it on.*

I opened the door and went full tilt. I was kind of scared to touch its mangy fur, so I kicked it around like a soccer ball. The hog had never seen such a formidable opponent, and quickly took its leave before I could put a sizable dent into its HP bar. That'll show em.

A flood of people exited the theater, and headed to God knows where, for there certainly were not enough cars to warrant such a crowd. My dream self thought nothing of it as Will & Matt snuck up on me.

"sup my dude!"

"nm hbu"

*Do not try this at home.