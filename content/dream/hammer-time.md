+++
categories = ["dreams"]
date = "2015-05-15T01:00:00-04:00"
description = "Wait, and it won't arrive. Pursue, and you won't find."
disqus_identifier = "/dream//"
disqus_title = ""
disqus_url = "https://dreams.poeti.codes/post//"
images = ["./sandis.png"]
tags = ["lost", "videogames", "healer", "dazed and confused", "adventure"]
title = "Hammer Time"
toc = false

+++
A righteous wave   
began to form   
just beyond my reach.

My righteous board   
began to turn   
as I paddled   
toward the beach.

But all that's right   
left my board   
at the bottom   
of a coral reef.

The reef extended into walls   
which expanded into roofs   
which gave form to a maze. 

And this is when the perspective changed   
and I began to play a videogame!

--- DREAM SEQUENCE CHANGE ---

I had a large hammer for a weapon, but I despised it. In my heart, I knew I had to seek out the weapon made for me. And thusly I fought and dove deeper into the maze.

A man challenged me to a duel, and I accepted. My slow hammer could hardly keep up with his movement, which truly upset me, as this man was not all that great in combat. We reached a stalemate, or rather, when the first opportunity presented itself, I ran as far away from him as I possibly could.

After some time, my phone began to ring. I answered, and it was a good friend. She wondered where I was. I told her my surroundings: lots of multi-colored walls. Doors everywhere with giant numbers, ordered sequentially. The closest door to me was #300. She told me her closest door, but I promptly forgot what it was.

We agreed to meet up at the door precisely in the middle of both numbers. She hung up, and I continued my search for my favored weapon. Hopefully her number was higher than mine.

Each little enemy that I flattened with my hammer dropped gold coins and cursed weapons. They all had purple auras, and gave off bad vibes. The gold coins were fine, besides the usual why-do-i-care-so-much-about-money vibes that we all experience from time to time (to be more precise: when bills are due).

Suddenly, after I thwomped a poor little goomba-guy, a weapon with a gold aura and why-do-i-care-so-much-about-money vibes appeared. Yes, it was a Healer's Wand!

With my favored weapon in my hands, I felt a different emptiness in my heart. A healer is of no use should they have no one to heal.

I felt bad about ditching my friend.  
I felt bad about thwomping that goomba.  
I felt bad about caring so much about money.

I woke up, and the guilt stuck with.