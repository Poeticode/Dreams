+++
categories = ["dreams", "mafia", "gangsters", "thuglife", "world of warcraft", "initiation", "accident-prone"]
date = "2018-01-27T15:16:38Z"
description = "Life is but a dream."
disqus_identifier = "/dream//"
disqus_title = ""
disqus_url = "https://dreams.poeti.codes/post//"
images = ["sandis.png"]
tags = []
title = "Blood In, Blood Out"
toc = false

+++
I was in World of Warcraft, controlling my character from a third-person view. I was up to no good, and wished to find myself some trouble. I began to flaunt my stuff, and challenged random people to duels left and right. They all politely declined, until I stumbled upon two guys who were also presumably up-to-no-good.

No good, indeed, we did. I cast a few healing-over-time spells on myself. As their health dwindled rapidly from my attacks, I felt unstoppable.

Until they stopped their attacks. And laughed.

Laughter is never a good sign during the heat of battle. If it's your opponent, they're laughing at your misfortune. If it's your partner, they're laughing at your misfortune. Or they're about to betray you, and are laughing at your misfortune. If it's you laughing, then your heart is turning black as you laugh at another's misfortune, which is unfortunate.

And so now you should understand the gravity of the situation.

One of the laughing guys began charging up a spell as an ominous circle formed around me. Once the circle completed, I felt powerless. I could no longer attack them. And once my healing-over-time spells wore off, I couldn't recast them. They put a stop to the unstoppable, and before I knew it, they had won the duel.

They felt bad about the brutal beatdown, and offered to teach me their ways. I gladly obliged. I saw a Quest Accepted dialogue box pop up, and I changed to a first-person view upon clicking confirm.

"Kewl, my dude. Our master is a few blocks this way."

"Oh, and stay behind us. He doesn't take too kindly to visitors."

We soon approached an old brick apartment complex, and bolted up the stairs. One guy motioned for us to stay back, as he carried on forward down the hall. The guy who stayed with me then went down a few stair-steps and ducked, so as to not be within view from the hallway.

I followed suit, but took just a second too long to hide.

"Who in God's name is that?!" I heard from down the hall. I felt a needle pierce my chest, and my body froze. He shot me with a tranquilizer.

Terror arose as I heard his shoes click-clack towards my direction. Once he made it to my stair-step, I saw that he wore a purple suit and had a crazy look in his eyes.

He also held a pistol. Then held it to my face. Then asked a simple question.

"Who in God's name are you?"

I think I took a second too long to satisfy his inquiry. Honestly, I kinda zoned out and pondered the phrasing of the question. God's name, eh? Who are you to invoke that?

He shot me. Not in the head, thankfully, as I would've woken up from that. No, he shot me in the chest, and it felt warm.

The crazy man was perplexed when he saw that the shot hardly phased me. He slowly brought the pistol up to my temple.

"Hey, please take my watch." I said to him. It was a cat-themed watch with a purple strap, which seemed like his jam. He remained perplexed as he took the watch off my wrist.

"You're alright, kid."

I wasn't alright, and probably needed to go to the hospital, but I gladly accepted the compliment and smiled back at him. He helped me up and showed me around his large apartment room.

"I need some help around here."

Like perhaps cleaning up the blood splattered across your stairs?

"Here, take out the trash." and pointed at a corner full of weird wires and metals, with a bin that obviously needed to be filled with said weird materials and taken outside.

While cleaning up, I saw a few other poor saps trapped in this guy's apartment. One of them wheeled around a metal tray table full of explosives. She whispered, "We're looking for the power supply that powers these explosives, so that we might escape!"

I nodded in understanding, but had no intention to ever help in their search. For one, a power supply wouldn't set off explosives. You just need fire. B, I didn't know how powerful said explosives were, and would rather not have a guilty conscious if it were to harm/kill innocent bystanders. Lastly, I already had my own plan in the works to escape the crazy man.

Indeed, after filling up the bin, I hollered at the crazy man, "Yo, I'm taking this to the curb." and he gave a thumbs up in response. So I casually strolled down the hallway carrying this heavy bin of metal. I stopped before the stairs and took a long breath.

I took a step, and my foot completely missed its mark. Instead of solid ground, it met only with air. I had fully committed to the motion, and thus promptly fell. My head met with sharp metal. My vision went black. A second later, the beautiful colors of reality.

I technically made it out of there alive, so that's good enough for me!