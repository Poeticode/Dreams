+++
title = "{{ replace .TranslationBaseName "-" " " | title }}"
date = {{ .Date }}
description = "Life is but a dream."
draft = true
toc = false
disqus_identifier = "/dream/{{title}}/"
disqus_title = {{title}}
disqus_url = "https://dreams.poeti.codes/post/{{title}}/"
categories = ["dreams"]
tags = ["introduction"]
images = [
  "sandis.png"
] # overrides the site-wide open graph image
+++
