#! /bin/bash
# Publish to IPFS!

rm -r published/
mkdir published

hugo --destination published/
node_modules/sw-precache/cli.js --root=published

# add to ipfs
hash=$(ipfs add -r -q published/ | tail -n 1)

# pin new version
pin=$(ipfs pin add $hash)

# publish hash to ipns
ipns=$(ipfs name publish $hash)

echo $ipns
