/**
 * Copyright 2016 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

// DO NOT EDIT THIS GENERATED OUTPUT DIRECTLY!
// This file should be overwritten as part of your build process.
// If you need to extend the behavior of the generated service worker, the best approach is to write
// additional code and include it using the importScripts option:
//   https://github.com/GoogleChrome/sw-precache#importscripts-arraystring
//
// Alternatively, it's possible to make changes to the underlying template file and then use that as the
// new base for generating output, via the templateFilePath option:
//   https://github.com/GoogleChrome/sw-precache#templatefilepath-string
//
// If you go that route, make sure that whenever you update your sw-precache dependency, you reconcile any
// changes made to this original template file with your modified copy.

// This generated service worker JavaScript will precache your site's resources.
// The code needs to be saved in a .js file at the top-level of your site, and registered
// from your pages in order to be used. See
// https://github.com/googlechrome/sw-precache/blob/master/demo/app/js/service-worker-registration.js
// for an example of how you can register this script and handle various service worker events.

/* eslint-env worker, serviceworker */
/* eslint-disable indent, no-unused-vars, no-multiple-empty-lines, max-nested-callbacks, space-before-function-paren, quotes, comma-spacing */
'use strict';

var precacheConfig = [["404.html","6678c342701d5376939df1f35ab47ada"],["bpg/cinemagraph-6.bpg","e65c40ee845acf03ab86db9200c00bfc"],["categories/accident-prone/index.html","72c623a6bc32d6d8c6e030cbdda35e03"],["categories/accident-prone/index.xml","1ff7eda663bf42b462738a36f3cd8236"],["categories/accident-prone/page/1/index.html","5389f98fd352d5db068ac5c2d1eaec59"],["categories/dreams/index.html","54d8763197f5a975d6cf62d404d2028c"],["categories/dreams/index.xml","c7a796ba6a936df143f2987473b3f075"],["categories/dreams/page/1/index.html","7946d3aae3eff8d1e98eee4ff5b3222a"],["categories/gangsters/index.html","1c9a78450ef82a9afb3667cacf7ca4cc"],["categories/gangsters/index.xml","1320de37287f7bd93a332d010eccfb90"],["categories/gangsters/page/1/index.html","4759e7c1c0ff8ea4e3dd7b87b9e299e7"],["categories/index.html","6d5d20cd65a4e9f026f4656642f781ac"],["categories/index.xml","afdbdb02dadc8bb01fbf58fd1eb44539"],["categories/initiation/index.html","f999c26ecf74ccf1ee62782a96159a69"],["categories/initiation/index.xml","4ffdcd73eb770b0ea3840874fcb03b35"],["categories/initiation/page/1/index.html","4ac7c074d26af0f9515bd93e8077e97c"],["categories/mafia/index.html","5c5d18997d5916887f122e6dc39c8a8e"],["categories/mafia/index.xml","211c7a9cc5abccffbd506f4a0beea3d3"],["categories/mafia/page/1/index.html","92361d269509adfb8052bb93f60bae11"],["categories/thuglife/index.html","7683a9d3b34ace5f23c33e915ba0a849"],["categories/thuglife/index.xml","c3e459697f77023acf64f97c36c32eb1"],["categories/thuglife/page/1/index.html","c8131097d4820a864642a20f043c43a5"],["categories/world-of-warcraft/index.html","34ada284491eea2ce04fffef60ebd159"],["categories/world-of-warcraft/index.xml","a34017d9e34b9be890353387554649a4"],["categories/world-of-warcraft/page/1/index.html","d082d1eeacbdccdf1f92fbaef626137c"],["dream/blood-in--blood-out/index.html","0d002ce7ed1c2ce10b6150bda3965ae8"],["dream/hammer-time/index.html","a5070cf0709a20aed9f930418c6a0a8b"],["dream/index.html","88c1b9f78d248043438743e43bc4a785"],["dream/index.xml","4bd2a155b00e5149cd90aba2149a2854"],["dream/page/1/index.html","54120441a96fbf84e0ced49bf37c3654"],["dream/pigging-out/index.html","24be97ff938561142353e6a96f29614a"],["index.html","0a79bf933782776639987cd79d211130"],["index.xml","e468fff1f5a07ecc85e93e24aed13e82"],["js/bpgdec.js","2b269422d3ccedc9462224c08f19e9ba"],["js/bpgdec8.js","3403e8d81c1df2d20c86ba7df5344fae"],["js/bpgdec8a.js","32e78b98ed0b9f1c5306f2659ffda518"],["js/lazysizes.min.js","272f1f4eab6f55af96ddbd7f1f0fdf9d"],["js/service-worker-registration.js","d60f01dc1393cbaaf4f7435339074d5e"],["js/smoothscroll.js","9e94a12f02dd7b5de81afa0990122513"],["manifest.json","7c4e82cee1d3da70cf5a7dfdeb19545d"],["page/1/index.html","ed575420bfc9eedbceb79df2a976fd5a"],["robots.txt","ca121b5d03245bf82db00d14cee04e22"],["sandis_192.png","7796cdf5eae043b782acbc3fd060bf25"],["sitemap.xml","0f4202d5282be5c920e57da313ee39c3"],["tags/adventure/index.html","2901995858fb966e37d370fb125cb1ed"],["tags/adventure/index.xml","2b29e78d1239b6ab3cb76e00e2ba5af9"],["tags/adventure/page/1/index.html","d8038e1a4046fe90f9658aae31bf0b37"],["tags/dazed-and-confused/index.html","1357c51c5a7b2133a32877da4343359c"],["tags/dazed-and-confused/index.xml","666235e480c5d0adb0a18506aea2ec3d"],["tags/dazed-and-confused/page/1/index.html","7a36e71f8f1f5d13a2048c089ad89578"],["tags/healer/index.html","eaca9907e412cc518e46eb9b2438a61e"],["tags/healer/index.xml","bd717ec6b91b5838f2a228e1b3c3c667"],["tags/healer/page/1/index.html","8a72f2ef4ccc627e770dde7f6de61c21"],["tags/hog/index.html","78a44d6146368b19cc07f3fb8c89dc9c"],["tags/hog/index.xml","76b1cf5636e0b9c34dbc7f11a625967a"],["tags/hog/page/1/index.html","efeb5346759b9ac5e21bbc477581ea72"],["tags/index.html","142b95a9de958620fb412389b3e6ffc9"],["tags/index.xml","fec053eda4e346f423b95be8789675b6"],["tags/late/index.html","694f39d3562c171a4be55b3a9c948c51"],["tags/late/index.xml","56869b546a6ccc6374ac1677d8e8a2e8"],["tags/late/page/1/index.html","05b106d47b39f2b9960edbddac37e455"],["tags/lost/index.html","ab9423181cb3e70bfe56e889d6fac8d7"],["tags/lost/index.xml","0e8c2366105f7f1fd1481fc6f38e5b65"],["tags/lost/page/1/index.html","4580c4f333de369c3b89327713cde23a"],["tags/parking/index.html","80d39cd99ccbad9033a331fbea8c1e2f"],["tags/parking/index.xml","524a493e832717a579f5962c8e663a45"],["tags/parking/page/1/index.html","db5d1eab9513dff0dc84f81cdc41e1e3"],["tags/quarters/index.html","6f204cc079b472867d7bc0e12fded433"],["tags/quarters/index.xml","32355cb943e624d45e4fcad4b3d9dbd8"],["tags/quarters/page/1/index.html","bbed09aa16d51be9ed21c8a381c225e8"],["tags/theater/index.html","99d80e6d3ab37cb65dc0665cd8145bea"],["tags/theater/index.xml","c336265b9a074cb58ce00351f5a743bb"],["tags/theater/page/1/index.html","adf3c781474a38f3b20845bbdb1a6158"],["tags/videogames/index.html","0f20902cc45d51a7640fc9d2f7f81de0"],["tags/videogames/index.xml","f395ec243640be4f12951382d43f91be"],["tags/videogames/page/1/index.html","382e02f5180e6085cf6e62a266c53425"]];
var cacheName = 'sw-precache-v3-sw-precache-' + (self.registration ? self.registration.scope : '');


var ignoreUrlParametersMatching = [/^utm_/];



var addDirectoryIndex = function (originalUrl, index) {
    var url = new URL(originalUrl);
    if (url.pathname.slice(-1) === '/') {
      url.pathname += index;
    }
    return url.toString();
  };

var cleanResponse = function (originalResponse) {
    // If this is not a redirected response, then we don't have to do anything.
    if (!originalResponse.redirected) {
      return Promise.resolve(originalResponse);
    }

    // Firefox 50 and below doesn't support the Response.body stream, so we may
    // need to read the entire body to memory as a Blob.
    var bodyPromise = 'body' in originalResponse ?
      Promise.resolve(originalResponse.body) :
      originalResponse.blob();

    return bodyPromise.then(function(body) {
      // new Response() is happy when passed either a stream or a Blob.
      return new Response(body, {
        headers: originalResponse.headers,
        status: originalResponse.status,
        statusText: originalResponse.statusText
      });
    });
  };

var createCacheKey = function (originalUrl, paramName, paramValue,
                           dontCacheBustUrlsMatching) {
    // Create a new URL object to avoid modifying originalUrl.
    var url = new URL(originalUrl);

    // If dontCacheBustUrlsMatching is not set, or if we don't have a match,
    // then add in the extra cache-busting URL parameter.
    if (!dontCacheBustUrlsMatching ||
        !(url.pathname.match(dontCacheBustUrlsMatching))) {
      url.search += (url.search ? '&' : '') +
        encodeURIComponent(paramName) + '=' + encodeURIComponent(paramValue);
    }

    return url.toString();
  };

var isPathWhitelisted = function (whitelist, absoluteUrlString) {
    // If the whitelist is empty, then consider all URLs to be whitelisted.
    if (whitelist.length === 0) {
      return true;
    }

    // Otherwise compare each path regex to the path of the URL passed in.
    var path = (new URL(absoluteUrlString)).pathname;
    return whitelist.some(function(whitelistedPathRegex) {
      return path.match(whitelistedPathRegex);
    });
  };

var stripIgnoredUrlParameters = function (originalUrl,
    ignoreUrlParametersMatching) {
    var url = new URL(originalUrl);
    // Remove the hash; see https://github.com/GoogleChrome/sw-precache/issues/290
    url.hash = '';

    url.search = url.search.slice(1) // Exclude initial '?'
      .split('&') // Split into an array of 'key=value' strings
      .map(function(kv) {
        return kv.split('='); // Split each 'key=value' string into a [key, value] array
      })
      .filter(function(kv) {
        return ignoreUrlParametersMatching.every(function(ignoredRegex) {
          return !ignoredRegex.test(kv[0]); // Return true iff the key doesn't match any of the regexes.
        });
      })
      .map(function(kv) {
        return kv.join('='); // Join each [key, value] array into a 'key=value' string
      })
      .join('&'); // Join the array of 'key=value' strings into a string with '&' in between each

    return url.toString();
  };


var hashParamName = '_sw-precache';
var urlsToCacheKeys = new Map(
  precacheConfig.map(function(item) {
    var relativeUrl = item[0];
    var hash = item[1];
    var absoluteUrl = new URL(relativeUrl, self.location);
    var cacheKey = createCacheKey(absoluteUrl, hashParamName, hash, false);
    return [absoluteUrl.toString(), cacheKey];
  })
);

function setOfCachedUrls(cache) {
  return cache.keys().then(function(requests) {
    return requests.map(function(request) {
      return request.url;
    });
  }).then(function(urls) {
    return new Set(urls);
  });
}

self.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return setOfCachedUrls(cache).then(function(cachedUrls) {
        return Promise.all(
          Array.from(urlsToCacheKeys.values()).map(function(cacheKey) {
            // If we don't have a key matching url in the cache already, add it.
            if (!cachedUrls.has(cacheKey)) {
              var request = new Request(cacheKey, {credentials: 'same-origin'});
              return fetch(request).then(function(response) {
                // Bail out of installation unless we get back a 200 OK for
                // every request.
                if (!response.ok) {
                  throw new Error('Request for ' + cacheKey + ' returned a ' +
                    'response with status ' + response.status);
                }

                return cleanResponse(response).then(function(responseToCache) {
                  return cache.put(cacheKey, responseToCache);
                });
              });
            }
          })
        );
      });
    }).then(function() {
      
      // Force the SW to transition from installing -> active state
      return self.skipWaiting();
      
    })
  );
});

self.addEventListener('activate', function(event) {
  var setOfExpectedUrls = new Set(urlsToCacheKeys.values());

  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return cache.keys().then(function(existingRequests) {
        return Promise.all(
          existingRequests.map(function(existingRequest) {
            if (!setOfExpectedUrls.has(existingRequest.url)) {
              return cache.delete(existingRequest);
            }
          })
        );
      });
    }).then(function() {
      
      return self.clients.claim();
      
    })
  );
});


self.addEventListener('fetch', function(event) {
  if (event.request.method === 'GET') {
    // Should we call event.respondWith() inside this fetch event handler?
    // This needs to be determined synchronously, which will give other fetch
    // handlers a chance to handle the request if need be.
    var shouldRespond;

    // First, remove all the ignored parameters and hash fragment, and see if we
    // have that URL in our cache. If so, great! shouldRespond will be true.
    var url = stripIgnoredUrlParameters(event.request.url, ignoreUrlParametersMatching);
    shouldRespond = urlsToCacheKeys.has(url);

    // If shouldRespond is false, check again, this time with 'index.html'
    // (or whatever the directoryIndex option is set to) at the end.
    var directoryIndex = 'index.html';
    if (!shouldRespond && directoryIndex) {
      url = addDirectoryIndex(url, directoryIndex);
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond is still false, check to see if this is a navigation
    // request, and if so, whether the URL matches navigateFallbackWhitelist.
    var navigateFallback = '';
    if (!shouldRespond &&
        navigateFallback &&
        (event.request.mode === 'navigate') &&
        isPathWhitelisted([], event.request.url)) {
      url = new URL(navigateFallback, self.location).toString();
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond was set to true at any point, then call
    // event.respondWith(), using the appropriate cache key.
    if (shouldRespond) {
      event.respondWith(
        caches.open(cacheName).then(function(cache) {
          return cache.match(urlsToCacheKeys.get(url)).then(function(response) {
            if (response) {
              return response;
            }
            throw Error('The cached response that was expected is missing.');
          });
        }).catch(function(e) {
          // Fall back to just fetch()ing the request if some unexpected error
          // prevented the cached response from being valid.
          console.warn('Couldn\'t serve response for "%s" from cache: %O', event.request.url, e);
          return fetch(event.request);
        })
      );
    }
  }
});







